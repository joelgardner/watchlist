//
//  LoadingViewController.m
//  Watchlist
//
//  Created by Joel Gardner on 1/5/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "LoadingViewController.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController
{
    UIImageView *logoView;
}

//@synthesize rootViewController;

- (id)init
{
    self = [super init];
    if (self)
    {
        //logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"watchlistlogo.png"]];
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    //logoView.frame = CGRectMake(self.view.frame., <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    //[self.view addSubview:logoView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
