//
//  RootViewController.m
//  Watchlist
//
//  Created by Joel Gardner on 1/5/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "RootViewController.h"
#import "LoadingViewController.h"
#import "Watchlist-Prefix.pch"
#import "AddWatcherViewController.h"

@interface RootViewController ()
{
   UIScrollView *scrollView;
   LoadingViewController *loadingController;
   AddWatcherViewController *addWatcherViewController;
   UINavigationBar *navBar;
   UINavigationItem *navItem;
   UIToolbar *toolBar;
}
@end

@implementation RootViewController

@synthesize appDelegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        loadingController = [[LoadingViewController alloc] init];
       
        // create the navigation bar
        CGRect frame = [[UIScreen mainScreen] bounds];
        frame.size.height = NAV_BAR_HEIGHT;
        frame.origin.y -= NAV_BAR_HEIGHT;
        navBar = [[UINavigationBar alloc] initWithFrame:frame];
        [navBar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
        
        // create the logo view
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"watchlistlogo.png"]];
        frame = navBar.frame;
        frame.origin.x = (frame.size.width / 2) - (logoView.image.size.width / 2);
        frame.origin.y = (frame.size.height / 2) - (logoView.image.size.height / 2) + 4;
        frame.size = logoView.image.size;
        logoView.frame = frame;
        [navBar addSubview:logoView];
        
        // create the toolbar
        frame = [[UIScreen mainScreen] bounds];
        frame.origin.y = frame.size.height;
        frame.size.height = TOOL_BAR_HEIGHT;
        toolBar = [[UIToolbar alloc] initWithFrame:frame];
        [toolBar setBackgroundImage:[UIImage imageNamed:@"footer.png"]
                 forToolbarPosition:UIToolbarPositionBottom
                         barMetrics:UIBarMetricsDefault];
        
        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *gearsImage = [UIImage imageNamed:@"gears.png"];
        [settingsButton setBackgroundImage:gearsImage forState:UIControlStateNormal];
        [settingsButton setBackgroundImage:gearsImage forState:UIControlStateHighlighted];
        [settingsButton addTarget:self action:@selector(userTappedSettingsButton:) forControlEvents:UIControlEventTouchUpInside];
        settingsButton.frame = CGRectMake(10, 10, gearsImage.size.width, gearsImage.size.height);
        
        UIView *settingsButtonView = [[UIView alloc] initWithFrame:settingsButton.frame];
        settingsButtonView.bounds = CGRectOffset(settingsButtonView.bounds, 15, 20);
        [settingsButtonView addSubview:settingsButton];
        UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButtonView];
        [toolBar setItems:[NSArray arrayWithObject:settingsBarButton] animated:YES];
        
        // create the add button
        UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *addButtonImageNormal = [UIImage imageNamed:@"button-add-normal.png"];
        UIImage *addButtonImageActive = [UIImage imageNamed:@"button-add.png"];
        [addButton setBackgroundImage:addButtonImageNormal forState:UIControlStateNormal];
        [addButton setBackgroundImage:addButtonImageActive forState:UIControlStateHighlighted];
        [addButton addTarget:self action:@selector(userTappedAddButton:) forControlEvents:UIControlEventTouchUpInside];
        addButton.frame = CGRectMake(0, 0, addButtonImageNormal.size.width, addButtonImageNormal.size.height);
        
        // intermediate view to allow faux-positioning of UIBarbuttonItem via the CGRectOffset call below
        UIView *addButtonView = [[UIView alloc] initWithFrame:addButton.frame];
        addButtonView.bounds = CGRectOffset(addButtonView.bounds, 4, 10);
        [addButtonView addSubview:addButton];
        
        // create the root navigation item (which contains only the add button)
        navItem = [[UINavigationItem alloc] initWithTitle:@"add"];
        navItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:addButtonView];
        navItem.title = @"";
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark View Lifecycle

- (void)loadView
{
   [super loadView];
   
   NSLog(@"%f", [[UIScreen mainScreen] bounds].size.height);
   // set background pattern
   [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern_129.gif"]]];
   
   // attach our UIScrollView to our root view
   CGRect frame = [[UIScreen mainScreen] bounds];
   frame.origin.y = NAV_BAR_HEIGHT - 2;
   frame.size.height -= (NAV_BAR_HEIGHT + TOOL_BAR_HEIGHT) - 2;
   scrollView = [[UIScrollView alloc] initWithFrame:frame];
   [self.view addSubview:scrollView];
   [self.view insertSubview:navBar aboveSubview:scrollView];
   [self.view insertSubview:toolBar aboveSubview:scrollView];
   
   addWatcherViewController = [[AddWatcherViewController alloc] init];
   addWatcherViewController.view.frame = scrollView.bounds;
   [scrollView addSubview:addWatcherViewController.view];
   // attach our LoadingViewController to the scrollView
   //loadingController.view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
   //[scrollView addSubview:loadingController.view];
   
   
   
   //scrollView.backgroundColor = [UIColor greenColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self showNavigationBar:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
   [navBar pushNavigationItem:navItem animated:YES];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Device Orientation

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark User Actions

- (void)userTappedAddButton:(id)sender
{
    //What are you looking for?
   //Where are you looking for it?
}

- (void)userTappedSettingsButton:(id)sender
{
    
}

#pragma mark Animations

- (void)showController:(UIViewController *)_viewController animated:(BOOL)animated
{
   
}

- (void)showNavigationBar:(BOOL)show animated:(BOOL)animated
{
    CGRect rect = navBar.frame;
    rect.origin.y += show ? NAV_BAR_HEIGHT : -NAV_BAR_HEIGHT;
    
    CGRect rect2 = toolBar.frame;
    rect2.origin.y += show ? -TOOL_BAR_HEIGHT : TOOL_BAR_HEIGHT;
    if (animated)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.30];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    }
    navBar.frame = rect;
    toolBar.frame = rect2;
    if (animated)
        [UIView commitAnimations];
}

@end
