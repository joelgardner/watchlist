//
//  AddWatcherTableCell.h
//  Watchlist
//
//  Created by Joel Gardner on 1/25/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum AddWatcherTableCellQuestionType {
   WhatAreYouLookingFor,
   WhereAreYouLooking,
   WhenWouldYouLikeUpdates
} AddWatcherTableCellQuestionType;

@interface AddWatcherTableCell : UITableViewCell <UITextFieldDelegate, UIPickerViewDelegate>
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIPickerView *locationPickerView;

@end
