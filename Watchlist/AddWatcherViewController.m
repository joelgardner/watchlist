//
//  AddMonitorViewController.m
//  Watchlist
//
//  Created by Joel Gardner on 1/16/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "AddWatcherViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AddWatcherTableCell.h"

@interface AddWatcherViewController ()
{
   UIScrollView *scrollView;
   UIView *dialogView;
   UIImageView *whatAreYouImage;
   UITextField *whatAreYouTextField;
   UIImageView *whereAreYouImage;
   UITextField *whereAreYouTextField;
   UIImageView *dialogBubble;
   UIImageView *threeDots;
   
   UITableView *tableView;
}
- (AddWatcherTableCell *)getCellWithType:(AddWatcherTableCellQuestionType)questionType;
@end

@implementation AddWatcherViewController
{
   NSMutableArray *selected;
}

- (id)init
{
   self = [super init];
   if (self)
   {
      scrollView = [[UIScrollView alloc] init];
      dialogView = [[UIView alloc] init];
      whatAreYouImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whatareyoulookingfor.png"]];
      whatAreYouTextField = [[UITextField alloc] init];
      whatAreYouTextField.backgroundColor = [UIColor whiteColor];
      whatAreYouTextField.layer.cornerRadius = 15;
      whatAreYouTextField.layer.borderColor = [[UIColor colorWithRed:0.33 green:0 blue:0 alpha:1] CGColor];
      whatAreYouTextField.layer.borderWidth = 2;
      whatAreYouTextField.clearButtonMode = UITextFieldViewModeAlways;
      whatAreYouTextField.font = [UIFont fontWithName:@"Helvetica" size:30];
      whatAreYouTextField.textColor = [UIColor darkGrayColor];
      whatAreYouTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 28, 8)];
      whatAreYouTextField.leftViewMode = UITextFieldViewModeAlways;
      //whatAreYouTextField.placeholder = @"leather sofa";
      whatAreYouTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
      whatAreYouTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
      whatAreYouTextField.textAlignment = NSTextAlignmentCenter;
      whatAreYouTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
      whatAreYouTextField.returnKeyType = UIReturnKeyNext;
      whatAreYouTextField.delegate = self;
      dialogBubble = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whitedialogbackground.png"]];
      threeDots = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"threedots.png"]];
      
      tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
      tableView.delegate = self;
      tableView.dataSource = self;
      tableView.allowsMultipleSelection = YES;
      selected = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO], [NSNumber numberWithBool:NO], [NSNumber numberWithBool:NO], nil];
   }
   return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
   [super loadView];
   
   tableView.frame = self.view.bounds;
   tableView.backgroundColor = [UIColor clearColor];
   tableView.backgroundView = nil;
   [self.view addSubview:tableView];
   /*
   // add our scrollView
   scrollView.frame = self.view.bounds;
   [self.view addSubview:scrollView];
   
   // add the generic "holder" view for the white dialog bubble
   CGRect frame = CGRectMake(12, 100, dialogBubble.image.size.width, dialogBubble.image.size.height);
   dialogView.frame = frame;
   [scrollView addSubview:dialogView];
   
   // add the question image
   frame = CGRectMake(4, 4, whatAreYouImage.image.size.width, whatAreYouImage.image.size.height);
   whatAreYouImage.frame = frame;
   [dialogView addSubview:whatAreYouImage];
   
   // add the 3 dots image
   frame = CGRectMake((dialogBubble.frame.size.width / 2) - (threeDots.image.size.width / 2), (dialogBubble.frame.size.height / 2) - (threeDots.frame.size.height / 2) - 2, threeDots.image.size.width, threeDots.image.size.height);
   threeDots.frame = frame;
   [dialogView addSubview:threeDots];
   
   // add the text field
   frame = CGRectMake(4, dialogBubble.frame.size.height / 2 + 8, dialogBubble.frame.size.width - 10, (dialogBubble.frame.size.height / 2));
   whatAreYouTextField.frame = frame;
   [dialogView addSubview:whatAreYouTextField];
   //dialogBubble.clipsToBounds = YES;
    
    */
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate
/*
- (void)textFieldDidBeginEditing:(UITextField *)textField {
   textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
   textField.placeholder = @"leather sofa";
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   [textField resignFirstResponder];
   return YES;
}

#pragma mark UITableViewDataSource & UITableViewDelegate
/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)_section
{
   switch (_section) {
      case 0:
         return @"What are you looking for?";
      case 1:
         return @"Where are you?";
      case 2:
         return @"How often do you want updates?";
         
      default:
         break;
   }
   return nil;
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return [selected objectAtIndex:indexPath.row] == [NSNumber numberWithBool:YES] ? 90 : 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return 3;
}
/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   return whatAreYouImage.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   //CGRect frame = CGRectMake(0, 20, whatAreYouImage.image.size.width, whatAreYouImage.image.size.height);
   //whatAreYouImage.frame = frame;
   return whatAreYouImage;
}
*/
- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   //AddWatcherTableCell *c = (AddWatcherTableCell *)[_tableView dequeueReusableCellWithIdentifier:@"Cell"];
   //if (!c)
   {
      //enum AddWatcherTableCellQuestionType questionType;
      switch (indexPath.row) {
         case 0:  return [self getCellWithType:WhatAreYouLookingFor];
         case 1:  return [self getCellWithType:WhereAreYouLooking];
         case 2:  return [self getCellWithType:WhenWouldYouLikeUpdates];
         default: break;
      }
      //AddWatcherTableCell *c = [[AddWatcherTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell" questionType:questionType];
      //c.selectionStyle = UITableViewCellSelectionStyleNone;
      //c.textField.delegate = self;
      //return c;
   }
   
   /*
   if (indexPath.row == 0)
   {
      c.label.text = @"What are you looking for?";
      
      //c.textField.placeholder = @" ... are you looking for?";
   }
   else if (indexPath.row == 1)
   {
      c.label.text = @"Where are you?";
   }
   else if (indexPath.row == 2)
   {
      c.label.text = @"How often do you want updates?";
   }
   */
   //c.autoresizesSubviews = YES;
   //c.textLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
   //return c;
}
/*
- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell *c = [_tableView cellForRowAtIndexPath:indexPath];
   
   [selected setObject:[NSNumber numberWithBool:YES] atIndexedSubscript:indexPath.row];
   [UIView beginAnimations:nil context:nil];
   [UIView setAnimationDuration:0.30];
   [UIView setAnimationDelay:0.0];
   [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
   
   // expand our cell
   c.frame = CGRectMake(c.frame.origin.x, c.frame.origin.y, c.frame.size.width, c.frame.size.height + 40);
   
   // push each subsequent cell down
   int i = indexPath.row;
   while (++i < [_tableView numberOfRowsInSection:0])
   {
      c = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
      c.frame = CGRectMake(c.frame.origin.x, c.frame.origin.y + 40, c.frame.size.width, c.frame.size.height);
   }
   
   [UIView commitAnimations];
   [_tableView beginUpdates];
   [_tableView endUpdates];
}

- (void)tableView:(UITableView *)_tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell *c = [_tableView cellForRowAtIndexPath:indexPath];
   
   [selected setObject:[NSNumber numberWithBool:NO] atIndexedSubscript:indexPath.row];
   [UIView beginAnimations:nil context:nil];
   [UIView setAnimationDuration:0.30];
   [UIView setAnimationDelay:0.0];
   [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
   
   // contract our cell
   c.frame = CGRectMake(c.frame.origin.x, c.frame.origin.y, c.frame.size.width, c.frame.size.height - 40);
   
   // push each subsequent cell up
   int i = indexPath.row;
   while (++i < [_tableView numberOfRowsInSection:0])
   {
      c = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
      c.frame = CGRectMake(c.frame.origin.x, c.frame.origin.y - 40, c.frame.size.width, c.frame.size.height);
   }

   [UIView commitAnimations];
   [_tableView beginUpdates];
   [_tableView endUpdates];

}
 */

- (AddWatcherTableCell *)getCellWithType:(AddWatcherTableCellQuestionType)questionType
{
   AddWatcherTableCell *cell = [[AddWatcherTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
   int xOffset = 0;
   switch (questionType)
   {
      case WhatAreYouLookingFor:
         cell.textField.placeholder = @"are you looking for?";
         cell.imageView.image = [UIImage imageNamed:@"what.png"];
         xOffset = 7;
         break;
      case WhereAreYouLooking:
      {
         cell.textField.placeholder = @"are you looking?";
         cell.imageView.image = [UIImage imageNamed:@"where.png"];
         cell.locationPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 280)];
         cell.locationPickerView.delegate = self;
         cell.locationPickerView.showsSelectionIndicator = YES;
         cell.textField.inputView = cell.locationPickerView;
         NSString *path = [[NSBundle mainBundle] pathForResource:@"Locations" ofType:@"plist"];
         
         // Build the array from the plist
         NSDictionary *states = [[NSDictionary alloc] initWithContentsOfFile:path];
         NSLog(@"%@", states);
         break;
      }
      case WhenWouldYouLikeUpdates:
         cell.textField.placeholder = @"would you like updates?";
         cell.imageView.image = [UIImage imageNamed:@"when.png"];
         xOffset = 5;
         break;
      default:
         break;
   }
   
   
   cell.label.frame = CGRectMake(2, 0, 280, 20);
   cell.imageView.frame = CGRectMake(4 + xOffset, 30 - (cell.imageView.image.size.height / 2) - 2, cell.imageView.image.size.width, cell.imageView.image.size.height);
   cell.textField.frame = CGRectMake(84, 10, 214, 40);
   cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
   cell.textField.font = [UIFont fontWithName:@"Helvetica" size:17];
   cell.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
   cell.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
   //textField.backgroundColor = [UIColor purpleColor];
   //textField.te
   
   cell.label.font = [[cell textLabel] font];
   //label.textColor = [UIColor lightGrayColor];
   [cell.contentView addSubview:cell.imageView];
   //[self.contentView addSubview:label];
   [cell.contentView addSubview:cell.textField];
   return cell;
}

#pragma mark UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
   return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   switch(component)
   {
      case 0:
         return @"Texas";
      case 1:
         return @"Austin";
   }
   
   [NSException raise:@"invalid component id" format:@""];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
   switch (component)
   {
      case 0:
         return 100;
      case 1:
         return 90;
   }
   
   [NSException raise:@"invalid component id" format:@""];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
   return 2;
}

@end
