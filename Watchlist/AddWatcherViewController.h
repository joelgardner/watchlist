//
//  AddMonitorViewController.h
//  Watchlist
//
//  Created by Joel Gardner on 1/16/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddWatcherViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@end
