//
//  AddWatcherTableCell.m
//  Watchlist
//
//  Created by Joel Gardner on 1/25/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "AddWatcherTableCell.h"

@implementation AddWatcherTableCell

@synthesize label;
@synthesize imageView;
@synthesize textField;
@synthesize locationPickerView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
       label = [[UILabel alloc] init];
       imageView = [[UIImageView alloc] init];
       textField = [[UITextField alloc] init];
       
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
   [super setSelected:selected animated:animated];

   if (selected) {
      [textField becomeFirstResponder];
   }
}


@end
