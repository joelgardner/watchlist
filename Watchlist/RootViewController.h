//
//  RootViewController.h
//  Watchlist
//
//  Created by Joel Gardner on 1/5/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;

@interface RootViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, strong) AppDelegate *appDelegate;
@end
